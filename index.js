let http = require("http");

let courses = [
    {
        name:"Python 101",
        description:"Learn Python",
        price: 25000
    },
    {
        name:"ReactJS 101",
        description:"Learn React",
        price: 35000
    },
    {
        name:"ExpressJS 101",
        description:"Learn ExpressJS",
        price: 28000
    }
];

http.createServer(function(request,response){
    if(request.url === "/" && request.method === "GET"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Hello from our server. GET method");
    } else if(request.url === "/" && request.method === "POST"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Hello from our server. POST method");
    } else if(request.url === "/" && request.method === "PUT"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Hello from our server. PUT method");
    } else if(request.url === "/" && request.method === "DELETE"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Hello from our server. DELETE method");
    } else if(request.url === "/courses" && request.method === "GET"){
        response.writeHead(200,{"Content-Type":"application/json"});
        response.end(JSON.stringify(courses));
    } else if(request.url === "/courses" && request.method === "POST"){

        let requestBody = "";
        request.on('data',function(data){
            requestBody += data;
        })

        request.on('end', function(){
            requestBody = JSON.parse(requestBody);
            courses.push(requestBody);
            response.writeHead(200,{"Content-Type":"application/json"});
            response.end(JSON.stringify(courses));
        })
    }
}).listen(4000);

console.log("Server running...")